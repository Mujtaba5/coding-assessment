﻿using System;
using System.Collections.Generic;
using System.IO;

/*
 * NOTE: THE FILE PATH FOR data.csv, and results.txt is @"C:\Temp\", so please make sure that the data.csv is present in there or update the position of the folder location.
 */

/*
 * The main purpose of this program is to take a csv file and iterate through its data, and process through it, then update the variables accordingly. 
 * It has 3 dictionaries to keep track of the highest monthly revenue, the product with maximum quantity sold and the total revenue per each product. 
 * It updates the total revenue, and total quantity sold as it iterates.
 * Lastly for each loops to check for the products with maximum revenue, maximum quantity sold.
 */


namespace Store
{
    class Program
    {
        //To initialize the file path and read all the data into an array.
        static void openFile(ref string[] lines, ref StreamWriter ss)
        {
            string folder = @"C:\Temp\";
            string fileName = "results.txt";
            string sourceName = "data.csv";
            string fullPath = folder + fileName;
            string sourcePath = folder + sourceName;
            ss = new StreamWriter(fullPath);
            lines = System.IO.File.ReadAllLines(sourcePath);
        }

        //To do all the calculations required to fill in dictionaries.
        static void computeTotal(IDictionary<string, float> monthly, IDictionary<string, int> totalQt, IDictionary<string, float> totalRev, ref int totQty, ref float totRev, string[] lines)
        {
            for (int i = 1; i < lines.Length; i++)
            {
                string[] rowData = lines[i].Split(','); //split linej
                int x = Int32.Parse(rowData[1]); // to convert string to int.
                float y = float.Parse(rowData[2]);
                string[] rowMonth = rowData[3].Split('-');

                totQty += Int32.Parse(rowData[1]); //to convert string to int.
                totRev += x * y;
                try
                {
                    if (totalQt.ContainsKey(rowData[0])) // to check if the product has been added into the map.
                    {
                        totalQt[rowData[0]] += x;
                        totalRev[rowData[0]] += x * y;
                    }
                    else // if not added then add it.
                    {
                        totalQt.Add(rowData[0], x);
                        totalRev.Add(rowData[0], x * y);
                    }
                    if (monthly.ContainsKey(rowMonth[1]))
                    {
                        monthly[rowMonth[1]] += x * y;
                    }
                    else
                    {
                        monthly.Add(rowMonth[1], x * y);
                    }
                }
                catch(ArgumentException)
                {
                    Console.WriteLine("An element with Key already exists.");
                }
            }
        }

        static void printMax(IDictionary<string, float> monthly, IDictionary<string, int> totalQt, IDictionary<string, float> totalRev, StreamWriter sr)
        {

            int maxProd = 0;
            float maxMonthly = 0.0F;
            string maxProdName = "";
            string maxMonthlyName = "";

            foreach (KeyValuePair<string, int> prProd in totalQt)

            {
                sr.WriteLine("Product ID: {0}, with quantity sold: {1}", prProd.Key, prProd.Value);
                if (prProd.Value > maxProd)
                {
                    maxProd = prProd.Value;
                    maxProdName = prProd.Key;
                }
            }
            
            foreach (KeyValuePair<string, float> rev in totalRev)
            {
                sr.WriteLine("Product ID: {0}, with the total revenue earned from it: {1}", rev.Key, rev.Value);
            }

            foreach (KeyValuePair<string, float> kvp in monthly)
            {
                if (kvp.Value > maxMonthly)
                {
                    maxMonthly = kvp.Value;
                    maxMonthlyName = kvp.Key;
                }
            }
            sr.WriteLine("Most popular product based on the quantity sold with, Product ID: {0}, Quantity Sold: {1}", maxProdName, maxProd);
            sr.WriteLine("Month: {0}, with highest Monthly Revenue generated of: {1}", maxMonthlyName, maxMonthly);

        }

        static void Main(string[] args)
        {
            string[] csvLines = null;
            StreamWriter sw = null;
            openFile(ref csvLines, ref sw);

            IDictionary<string, float> monthRev = new Dictionary<string, float>(); //New Dictionaries for keeping track of each key and each value respectively as it iterates through the data.
            IDictionary<string, int> totalQtyPrProd = new Dictionary<string, int>(); // to keep track of quantity sold for each product
            IDictionary<string, float> totalRevPrProd = new Dictionary<string, float>(); // to keep track of revenue brought in by the particular product.
            int totalQty = 0;
            float totalRev = 0;
            computeTotal(monthRev, totalQtyPrProd, totalRevPrProd, ref totalQty, ref totalRev, csvLines);
            
            printMax(monthRev, totalQtyPrProd, totalRevPrProd, sw);

            sw.WriteLine("\nTotal Quantity Sold: {0}, Total Revenue generated: {1}", totalQty, totalRev);
            sw.Close();
            Console.WriteLine("All data has been updated into results.txt successfully");
        }

    }
}
